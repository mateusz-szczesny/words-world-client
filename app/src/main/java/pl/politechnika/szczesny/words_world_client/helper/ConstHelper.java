package pl.politechnika.szczesny.words_world_client.helper;

import java.util.regex.Pattern;

public class ConstHelper {
    public static final String USER__SP = "USER_SP";
    public static final String TOKEN__SP = "TOKEN_SP";
    public static final int MINIMUM_PASSWORD_LENGTH = 6;
    public static final int MINIMUM_USERNAME_LENGTH = 6;
}
